﻿using System;
using System.Windows.Forms;

namespace 串口上位机
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String[] ports = System.IO.Ports.SerialPort.GetPortNames();
            comboBox1.Items.AddRange(ports);
            comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
            comboBox5.SelectedIndex = 0;
            comboBox6.SelectedIndex = 0;
            comboBox7.SelectedIndex = 4;
            textBox3.Text = "0001";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "打开串口")     //如当前是串口设备是关闭状态
            {
                try                            
                {
                    serialPort1.PortName = comboBox1.Text;                   //将串口设备的串口号属性设置为  comboBox1复选框中选择的串口号
                    serialPort1.BaudRate = Convert.ToInt32(comboBox2.Text);  //将串口设备的波特率属性设置为  comboBox2复选框中选择的波特率
                    serialPort1.Open();     //打开串口，如果打开了继续向下执行，如果失败了，跳转至catch部分
                    comboBox1.Enabled = false;   //串口已经打开了，将comboBox1设置为不可操作
                    comboBox2.Enabled = false;   //串口已经打开了，将comboBox2设置为不可操作
                    button1.Text = "关闭串口";      //将串口开关按键的文字改为  “关闭串口”
                    button14_Click(sender, e);
                }
                catch
                {
                    MessageBox.Show("打开串口失败，请检查串口", "错误");   //弹出错误对话框
                }
            }
            else              //如果当前串口设备是打开状态
            {
                try
                {
                    serialPort1.Close();    //关闭串口
                    comboBox1.Enabled = true;   //串口已经关闭了，将comboBox1设置为可操作
                    comboBox2.Enabled = true;   //串口已经关闭了，将comboBox2设置为可操作
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
                catch
                {
                    MessageBox.Show("关闭串口失败，请检查串口", "错误");   //弹出错误对话框
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)    //如果串口设备已经打开了
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                try
                {
                    for (int i = 0; i < textBox2.Text.Length; i++)
                    {
                        str[0] = Convert.ToChar(textBox2.Text.Substring(i, 1));  //取待发送文本框中的第i个字符
                        serialPort1.Write(str, 0, 1);     //写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string groupNo = comboBox6.Text;
            //string str = serialPort1.ReadExisting();   //读取串口接收缓冲区字符串
            string str =  serialPort1.ReadLine();
            string[] sArray = str.Split('&');
            if (sArray[0].StartsWith(groupNo))
            {
                textBox1.AppendText(str + "");
                if (sArray[2] == "STATE")
                {
                    label32.Text = sArray[3];
                    label34.Text = sArray[4];
                    label11.Text = sArray[5];
                    label12.Text = sArray[6];
                    label13.Text = sArray[7];
                    label26.Text = sArray[8];
                    label23.Text = sArray[9];
                    label24.Text = sArray[10];
                    switch (sArray[11])
                    {
                        case "0":
                            comboBox7.SelectedIndex = 0;
                            break;
                        case "1":
                            comboBox7.SelectedIndex = 1;
                            break;
                        case "2":
                            comboBox7.SelectedIndex = 2;
                            break;
                        case "3":
                            comboBox7.SelectedIndex = 3;
                            break;
                        case "4":
                            comboBox7.SelectedIndex = 4;
                            break;
                    }
                    string[] mArray = sArray[12].Split('#');
                    if (mArray[0] == "1")
                    {
                        checkBox1.Checked = true;
                    } else if (mArray[0] == "0")
                    {
                        checkBox1.Checked = false;
                    }
                    if (mArray[1] == "1")
                    {
                        checkBox2.Checked = true;
                    }
                    else if (mArray[1] == "0")
                    {
                        checkBox2.Checked = false;
                    }
                    if (sArray[10] == "4")
                    {
                        comboBox7.SelectedIndex = 4;
                        textBox4.Text = sArray[13];
                        textBox5.Text = sArray[14];
                        textBox6.Text = sArray[15];
                    }
                    label41.Text = sArray[16];
                    label40.Text = sArray[17];
                    if (sArray[18] == "1")
                    {
                        label28.Text = "开";
                        button7.Text = "关闭";
                    } else if (sArray[18] == "0")
                    {
                        label28.Text = "关";
                        button7.Text = "开启";
                    }
                    if (sArray[19] == "1")
                    {
                        label36.Text = "开";
                        button10.Text = "关闭";
                    }
                    else if (sArray[19] == "0")
                    {
                        label36.Text = "关";
                        button10.Text = "开启";
                    }                  
                } else if (sArray[2] == "RESERVERED")
                {
                    return;
                }
            } else
            {
                return;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                string deviceNo = textBox3.Text;
                char[] str = new char[1];  //定义一个字符数组，只有一位
                char cb1 = '0';
                char cb2 = '0';
                if (checkBox1.Checked ) {
                    cb1 = '1';
                }
                if (checkBox2.Checked)
                {
                    cb2 = '1';
                }
                string control = "YZDX" + deviceNo + "&YZDXFFFF&MOTORSET:" + cb1 + cb2 + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }

                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                string deviceNo = textBox3.Text;
                char[] str = new char[1];  //定义一个字符数组，只有一位
                char temp = '0';
                switch(comboBox7.SelectedIndex)
                {
                    case 0:
                        temp = '0';
                        break;
                    case 1:
                        temp = '1';
                        break;
                    case 2:
                        temp = '2';
                        break;
                    case 3:
                        temp = '3';
                        break;
                    case 4:
                        temp = '4';
                        break;
                    default:
                        break;

                }
                string control = "YZDX" + deviceNo + "&YZDXFFFF&RUNMODLESET:" + temp + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string temp = DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss");
                string deviceNo = textBox3.Text;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&TIMESET:" + temp + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&RESET:" + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
                //经度
                string longitude = textBox7.Text;
                //纬度
                string latitude = textBox8.Text;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&LOCALSET:N" + longitude + " E" + latitude + "\r\n";                     
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return ;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
                int temp = label28.Text == "关" ? 1 : 0;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&CHARGESET:" + temp + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }           
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
                if (temp == 1)
                {
                    button7.Text = "关闭";
                    label28.Text = "开";
                }
                else
                {
                    button7.Text = "开启";
                    label28.Text = "关";
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
                int temp = label36.Text == "关" ? 1 : 0;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&HEATSET:" + temp + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
                if (temp == 1)
                {
                    button10.Text = "关闭";
                    label36.Text = "开";
                }
                else
                {
                    button10.Text = "开启";
                    label36.Text = "关";
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                string deviceNo = textBox3.Text;
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string cb4 = textBox4.Text;
                string cb5 = textBox5.Text;
                string cb6 = textBox6.Text;               
                string control = "YZDX" + deviceNo + "&YZDXFFFF&ANGLESET:" + cb4 + "#" + cb5 + "#" + cb6 + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void label37_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
               
                string temp = textBox9.Text;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&DEVICEIDSET:YZDX" + temp + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
                
                string temp1 = textBox10.Text;
                string temp2 = textBox11.Text;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&OFFSET:" + temp1 + "" + temp2 + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] str = new char[1];  //定义一个字符数组，只有一位
                string deviceNo = textBox3.Text;
                string control = "YZDX" + deviceNo + "&YZDXFFFF&R_STATE:" + "\r\n";
                //开始写入字符数组
                try
                {
                    for (int i = 0; i < control.Length; i++)
                    {
                        str[0] = Convert.ToChar(control.Substring(i, 1));
                        serialPort1.Write(str, 0, 1);     //将字符挨个写入串口设备进行发送
                    }
                }
                catch
                {
                    MessageBox.Show("命令出错!", "错误");   //弹出发送错误对话框
                    serialPort1.Close();             //关闭串口
                    button1.Text = "打开串口";    //将串口开关按键的文字改为  “打开串口”
                }
            }
            else
            {
                MessageBox.Show("串口未开启!", "错误");
                return;
            }
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox7.SelectedIndex == 4)
            {
                textBox4.Enabled = true;
                textBox5.Enabled = true;
                textBox6.Enabled = true;
            }
            else
            {
                textBox4.Enabled = false;
                textBox5.Enabled = false;
                textBox6.Enabled = false;
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
